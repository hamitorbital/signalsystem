import numpy as np
import matplotlib.pyplot as plt
from scipy import signal as sd

Xn=[]
Xn_1=[]
y=[]
n=[]
j=0

for i in range(0,200):
    n.append(i)
    Xn.append((np.sin((np.pi*i)/25))*(np.heaviside(i,1)-np.heaviside(i-100,1)))
    Xn_1.append((np.sin((np.pi*(i-1))/25))*(np.heaviside((i-1),1)-np.heaviside(i-1-100,1)))
    y.append(Xn[j]-Xn_1[j])
    j+=1
    
plt.stem(n,y)
plt.title("soal 2 >>C")
plt.show()