import numpy as np
import matplotlib.pyplot as plt

Xn=[]
Xn_1=[]
y=[]
n=[]
j=0
for i in range(-20,30):
    n.append(i)
    Xn.append(i*(np.heaviside(i,1)-np.heaviside((i-10),1))+(20-i)*(np.heaviside((i-10),1)-np.heaviside((i-20),1)))
    Xn_1.append((i-1)*(np.heaviside((i-1),1)-np.heaviside((i-1-10),1))+(20-(i-1))*(np.heaviside((i-1-10),1)-np.heaviside((i-1-20),1)))
    y.append(Xn[j]-Xn_1[j])
    j+=1
    
plt.stem(n,y)
plt.title("soal 2 B")
plt.show()