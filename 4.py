from scipy import signal
import matplotlib.pyplot as plt
import numpy as np

a=[1,-1.76,1.182,-0.2781]
b=[0.0181,0.0543,0.0543,0.0181]
w, h = signal.freqz(b,a,whole=True)
angles = np.unwrap(np.angle(h))
angles = np.angle(h)
plt.plot(w,h)
plt.show()
