from scipy import signal
import matplotlib.pyplot as plt
import numpy as np

N=np.arange(-20, 31, 1).tolist()
Xn=[]
Xn1=[]
for n in N:
    print(np.heaviside(n,1))
    Xn.append(5*(np.heaviside(n,1)-np.heaviside(n-20,1)))
    Xn1.append(5*(np.heaviside(n-1,1)-np.heaviside(n-21,1)))

y=[]
for i in range(0,len(N)):
    y.append(Xn[i]-Xn1[i])

plt.stem(N,y)
plt.show()
signal.lfilter