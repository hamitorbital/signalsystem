import numpy as np 
import matplotlib.pyplot as plt

n=[]
Xn=[]
Hn=[]

for i in range(-15,16):
    n.append(i)
    Xn.append((i/4)*(np.heaviside(i,1)-np.heaviside((i-6),1)))

for i in n:
    Hn.append(2*(np.heaviside(i+2,1)-np.heaviside(i-3,1)))

Yn=np.convolve(Xn,Hn,mode='same')
plt.stem(n,Yn)

plt.grid(True)
plt.title("soal 1")

plt.show()