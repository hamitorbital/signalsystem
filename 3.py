import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import lfilter


b=[1]
a=[1,-0.8]
Xn=[]
N=[]
for n in range(-100,200):
    Xn.append(np.cos(0.05*n*np.pi)*np.heaviside(n,1))
    N.append(n)

Y=lfilter(b,a,Xn)
plt.stem(N,Y)
plt.title("soal 3")
plt.show()
