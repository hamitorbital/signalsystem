clear all 
clc
sympref('HeavisideAtOrigin',1);
b=[1];
a=[1,-0.8];
n=[-100:200];
Xn=cos(0.05.*pi.*n).*heaviside(n);
Y = filter(b,a,Xn);
stem(Y)
